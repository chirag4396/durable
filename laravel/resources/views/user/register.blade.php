@extends('user.layouts.master')

@push('header')
@php
$ID = 'worker';

$empId = 'DFMS'.substr((\App\Models\UserDetail::max('ud_id') + 10000001),1,8);

@endphp
@push('header')
<script>
	ID = '{{ $ID }}';
</script>
<style type="text/css">

img {
	object-fit: contain;
}
</style>
<link href="{{ asset('css/select2.css') }}" rel="stylesheet" />

@endpush

@section('content')
<section class="fac-list">
	<div class="container fac-list1" id = "regForm">
		<h2 class="text-center job-header">Register here for {{ isset($name) ? ucwords(str_replace('-', ' ', $name)) : '' }} Job</h2>		
		@include('user.includes.'.str_replace('-', '_', $name).'_form', ['ID' => $ID, 'type' => $type, 'empId' => $empId])
	</div>
	<div class = "hidden" id = "success">
		<h1 class="text-center" style = "padding: 10%;">Welcome to Durable Facility Management Service Pvt. Ltd. Please note your Employee ID - <b>{{ $empId }}</b></h1>
	</div>
</section>
@endsection

@push('footer')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route($ID.'.store') }}',		
		processResponse : function (data) {
			console.log(data);
			if(data.msg == "success"){			
				var blank = '{{ asset("images/no-image.png") }}',
				userBlank = '{{ asset('images/blank-user.jpg') }}';
				$('.qua, .lang, .cat').val(null).trigger('change');			
				$('#user-imgPreview').attr('src' , userBlank);
				$('#id-cardPreview, #add-cardPreview').attr('src', blank);
				$('#regForm').addClass('hidden');
				$('#success').removeClass('hidden');
				$('html, body').animate({scrollTop : 0},600);
			}
		}
	});	
	function geturl(name) {
		var link = '{{ route('home') }}/'+name;
		return link;
	}

	multiSelect('.qua', 'qualification');
	multiSelect('.lang', 'language');
	multiSelect('.cat', 'sub-category');
	multiSelect('.tongue', 'language');
	multiSelect('.blood-group', 'blood-group');
	multiSelect('.cleaning', 'cleaning');
	multiSelect('.baby', 'baby-sitting');
	multiSelect('.food', 'food-region');
	$('.cook, .food_type').select2();
	multiSelect('.elderly', 'elderly-care');
	multiSelect('.location', 'location');
	multiSelect('.dishes', 'dish');
	multiSelect('.c-city', 'city');
	multiSelect('.c-location', 'location');
	multiSelect('.p-location', 'location');
	multiSelect('.p-city', 'city');
	imageUpload('user-img');

	$('input[type="checkbox"]').on({
		'change' : function(){
			if($(this).is(':checked')){
				$(this).parent().addClass('select-check');
			}else{
				$(this).parent().removeClass('select-check');
			}
		}
	});
</script>
@endpush
