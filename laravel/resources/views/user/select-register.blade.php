@extends('user.layouts.master')
@section('content')
<div class="portfolio">
	<div class="container">
		<h2 class="heading-agileinfo sub-head">
			<p>Select your job type for Registration</p>
		</h2>
		<div class="portfolio-agile">
			<div class="main">
				@forelse (\App\Models\Category::whereIn('cat_id',[1,2,4])->get() as $cat)
				<div class="view view-seventh">
					<a href="{{ route('job-register',['type' => $cat->cat_id, 'name' => str_replace(" ", "-", strtolower($cat->cat_title))]) }}">
						<img src="{{ asset($cat->cat_img_path) }}">					
						<label>
							<div class="mask">
								<h4>{{ $cat->cat_title }}</h4>
							</div>
						</label>
						<div>
							<h4>{{ $cat->cat_title }}</h4>
						</div>
					</a>
				</div>
				@empty

				@endforelse
				<div class="clearfix"></div>				
			</div>
		</div>
	</div>
</div>
@endsection

