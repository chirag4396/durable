@extends('user.layouts.master')

@push('header')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
<section class="fac-list">
	<div class="container fac-list1">		
		<div class="col-md-8 col-md-offset-2">
			<h2 class="text-center">Payment</h2>
			<form method="post" class="comments-form" action="{{ route('placeorder') }}">
				{{ csrf_field() }}
				<div class="form-group">
					<div class="col-md-4 no-padding">
						<label>Amount <span class="required">*</span></label>
						<input class="form-control pay-amount" type="number" min = "0" id = "amount"/><i class="fa fa-plus pay-symbol"></i>
					</div>			
					<div class="col-md-4 no-padding">						
						<label>Convenience Charge</label>
						<input class="form-control pay-amount" id = "convenience" name="amount" type="number" min = "0" readonly /><i class="fa fa-equals pay-symbol"></i>
					</div>
					<div class="col-md-4 no-padding">
						<label>Final Amount</label>					
						<input class="form-control" id = "total" name="amount" type="number" min = "0" value="{{ (empty($posted['amount'])) ? '' : $posted['amount'] }}" readonly />
					</div>
					<div class="clearfix"></div>
				</div>
				<input hidden name="productinfo" value="{{  (empty($posted['productinfo'])) ? 'demo' : $posted['productinfo'] }}" size="64" />

				<input hidden name="surl" value="{{ (empty($posted['surl'])) ? route('orderStatus') : $posted['surl'] }}" size="64" />
				<input hidden name="furl" value="{{  (empty($posted['furl'])) ? route('orderStatus') : $posted['furl']  }}" size="64" />

				
				<div style="margin-bottom: 10px;">
					<label class="control-label">Company(Optional)</label>
					<select name="company" class="company">
						<option value="-1">--Select--</option>
						@forelse (\App\Models\Company::get() as $c)
						<option value="{{ $c->cmp_id }}" {{ (empty($posted['company'])) ? '' : (($posted['company'] == $c->cmp_id ) ? 'selected' : '') }}>{{ $c->cmp_title }}</option>
						@empty								
						@endforelse
					</select>						
				</div>
				<div class="form-group">
					<label>Contact Person Name: <span class="required">*</span></label>	
					<input class="form-control" name="firstname" type="text" id="firstname" value="{{ (empty($posted['firstname'])) ? '' : $posted['firstname'] }}" />

				</div>
				<div class="form-group">
					<label>Mobile Number: <span class="required">*</span></label>
					<input class="form-control" type="text" name="phone" value="{{ (empty($posted['phone'])) ? '' : $posted['phone'] }}" />

				</div>
				<div class="form-group">
					<label>Email ID: <span class="required">*</span></label>
					<input class="form-control" name="email" type="text" id="email" value="{{ (empty($posted['email'])) ? '' : $posted['email'] }}" />

				</div>				
				
				<input type="hidden" name="udf1" value="{{ (empty($posted['udf1'])) ? '' : $posted['udf1'] }}" />
				
				<input type="hidden" name="udf2" value="{{ (empty($posted['udf2'])) ? '' : $posted['udf2'] }}" />
				
				
				
				<input type="hidden" name="udf3" value="{{ (empty($posted['udf3'])) ? '' : $posted['udf3'] }}" />
				
				<input type="hidden" name="udf4" value="{{  (empty($posted['udf4'])) ? '' : $posted['udf4'] }}" />
				
				
				
				<input type="hidden" name="udf5" value="{{ (empty($posted['udf5'])) ? '' : $posted['udf5'] }}" />
				<div class="form-group text-center">
					<input class = "btn btn-success" type="submit" value="Pay Now">
				</div>  
			</form>
			
		</div>
	</div>
</section>
@endsection

@push('footer')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
	$('.company').select2();
	$('#amount').on({
		'keyup change' : function(){
			var amt = parseFloat(this.value);
			var c = ((amt * 3.5) / 100);
			var total = amt + c;
			$('#convenience').val(c);
			$('#total').val(total);
		}
	});
</script>
@endpush
