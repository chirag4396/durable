
@include('user.includes.enquiry', ['category' => 0])
<div class="fix-button">	
	<a href = "tel:{{ config('app.mobile') }}" class="btn btn-danger btn-lg" title="Call us"><i class="fa fa-phone"></i></a>
	<button type="button" onclick="hire(0);" class="btn btn-primary btn-lg" title="Enquiry here"><i class="fa fa-envelope"></i></button>
</div>
@if (Route::currentRouteName() != "payment.create")	
<a href = "{{ route('get-pay') }}" id="myBtn" title="Pay your Bills here">Pay here</a>
@endif

<!-- footer start -->
<footer>
	<!-- footer-top-area start -->
	<div class="footer-top-area">
		<div class="container">
			<div class="row">
				<!-- footer-widget start -->
				<div class="col-lg-3 col-md-3 col-sm-4">
					<div class="footer-widget">
						<img src="{{ asset('images/logo.png') }}" alt="" width="100%" />
						<p></p>
						<div class="widget-icon">
							<a href="https://www.facebook.com/Durable-Facility-Management-Services-Pvt-Ltd-1503700849727172/"><i class="fa fa-facebook fb"></i></a>
							<a href="https://twitter.com/BarideSachin/"><i class="fa fa-twitter tw"></i></a>
							<a href="https://www.linkedin.com/in/sachin-baride-10392715b/"><i class="fa fa-linkedin ln"></i></a>
							<a href="https://plus.google.com/u/4/104692814929014250380/"><i class="fa fa-google-plus gp"></i></a>
							<a href="https://www.instagram.com/dfmspl/"><i class="fa fa-instagram instagram"></i></a>
						</div>
					</div>
				</div>
				<!-- footer-widget end -->
				<!-- footer-widget start -->
				<div class="col-lg-3 col-md-3 hidden-sm">
					<div class="footer-widget">
						<h3>Our services</h3>
						<p>We are India's largest online aggregator of maid bureaus. Whether you are looking for a maid or a nanny or a patient caretaker in All India. You will find the best quality and experienced staff here.</p>
					</div>
				</div>
				<!-- footer-widget end -->
				<!-- footer-widget start -->
				<div class="col-lg-3 col-md-3 col-sm-4">
					<div class="footer-widget">
						<h3>Useful Links</h3>
						<ul class="footer-menu">
							<li><a href="{{ route('home') }}">Home</a></li>
							<li><a href="{{ route('about') }}">About us</a></li>
							<li><a href="{{ route('contact.create') }}"> Contact us</a></li>
						</ul>
					</div>
				</div>
				<!-- footer-widget end -->
				<!-- footer-widget start -->
				<div class="col-lg-3 col-md-3 col-sm-4">
					<div class="footer-widget">
						<h3>CONTACT US</h3>
						<ul class="footer-contact">
							<li>
								<i class="fa fa-map-marker"> </i>
								Addresss: Pune, Maharashtra.
							</li>
							<li>
								<i class="fa fa-envelope"> </i>	
								Email: dfmspl1@gmail.com, support@dfmspl.com
							</li>
							<li>
								<i class="fa fa-phone"> </i>
								Phone: +91 77210 44526
							</li>
						</ul>
					</div>
				</div>
				<!-- footer-widget end -->
			</div>
		</div>
	</div>
	<!-- footer-top-area end -->
	<!-- footer-bottom-area start -->
	<div class="footer-bottom-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-6">
					<div class="copyright">
						<p>&copy; 2018 Facility . All Rights Reserved | Design by <a href="http://sungare.com/">Sungare</a>. All Rights Reserved</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<div class="payment-img">
						<img src="{{ asset('images/pay.png') }}" alt="Support All Payment Methods" width="100%" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- footer-bottom-area end -->
</footer>
<!-- footer end -->
<div><script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script></div>

<script src="{{ asset('admin-assets/js/crud.min.js') }}"></script>

<script src="{{ asset('js/responsiveslides.min.js') }}"></script>
<!-- gallery-pop-up-script -->
<script src="{{ asset('js/jquery.chocolat.min.js') }}"></script>
<!-- //gallery-pop-up-script -->

<script defer src="{{ asset('js/jquery.flexslider.min.js') }}"></script>

<script src="{{ asset('js/SmoothScroll.min.js') }}"></script>

<!--Start-slider-script-->
<script type="text/javascript">
	// You can also use "$(window).load(function() {"
	$(function () {
	// Slideshow 4
	$("#slider4").responsiveSlides({
		auto: true,
		pager:true,
		nav:false,
		speed: 400,
		namespace: "callbacks",
		before: function () {
			$('.events').append("<li>before event fired.</li>");
		},
		after: function () {
			$('.events').append("<li>after event fired.</li>");
		}
	});

});
	$(window).load(function(){
		$('.flexslider').flexslider({
			animation: "slide",
			start: function(slider){
				$('body').removeClass('loading');
			}
		});
	});
</script>


<script>
	$(window).on('load',function(){
		$('#loader').addClass('hidden');
	});
	window.onscroll = function() {scrollFunction()};

	function scrollFunction() {
		if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
			document.getElementById("myBtn").style.display = "block";
		} else {
			document.getElementById("myBtn").style.display = "none";
		}
	}
</script>
<script type="text/javascript">
	function googleTranslateElementInit() {
		new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
	}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
@stack('footer')
</body>	
</html>