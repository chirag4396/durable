@extends('user.layouts.master')

@push('header')
@php
$ID = 'enquiry';
@endphp
<script type="text/javascript">
	var ID = '{{ $ID }}';
</script>
<link href="{{ asset('css/select2.css') }}" rel="stylesheet" />
@php
$cats = \App\Models\SubCategory::whereIn('scat_id',explode(',',$category))->get();
$t = [];
$mc = [];
foreach ($cats as $k => $v) {		
	$t[] = $v->scat_title;
	$mc[] = $v->scat_cat_id;
}
$title = implode(', ',$t);

@endphp
@endpush

@section('content')
@if ($category == '35')

<div class="container">
	<h2 class="heading-agileinfo sub-head security-h">					
		<span class="security-h-span">We supply trained security guards and officers at various places in India such as Big Housing Societies, Various Companies, Banks, Corporate offices, Hotels, Colleges, Hostels, Hospitals and in every quarter of India where there is a need for trained Security guards.</span>
	</h2>		
</div>
@endif

<section class="fac-list">
	<div class="container fac-list1">
		<div class="col-md-8 col-xs-12 ">
			<div class = "sub-banner" style="background: url('{{ asset($cats->first()->scat_img_path) }}');"></div>
			<div class="col-xs-12 f-title">
				<div class="col-md-6"><h4>{{ $title  }}</h4></div>
				<div class="col-md-2"><button type="button" onclick="hire(0);">Enquiry</button></div>
				<div class="col-md-4 st"><strong>{{ number_format($users->count()*123) }}: People </strong> Available <img src="{{ asset('images/qq.png') }}"></div>
			</div>
			<div class="clearfix"></div>
			<div>
				<div class="alert text-center not-hire">
					<p>You are not Authorized to view Employees, please contact us for Hirings.</p>
					<button type="button" onclick="hire(0);">Enquiry</button>
				</div>
			</div>

			{{-- Logic to show users --}}
			<div class = "hidden">
			{{-- <table class="table table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Gender</th>						
						<th>Particular Hiring</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($users as $k => $user)						
					<tr>
						<td>{{ ++$k }}</td>
						<td>{{ $user->user->name }}</td>
						<td>{{ $user->user->gender ? 'Female' : 'Male' }}</td>						
						<td><button type="button" onclick="hire({{ $user->ud_user }}, '{{ $user->user->name }}');">Hire Me</button></td>
					</tr>
					@empty
					<tr>
						<td colspan="4"> 
							<h3 class="text-center">No job seeker is available for {{ $cat->scat_title }}, still you can enquiry for them.</h3>
							<button type="button" onclick="hire(0);">Enquiry for {{ $cat->scat_title }}</button>
						</td>
					</tr>
					@endforelse				
				</tbody>
			</table> --}}
		</div>			
	</div>
	
	@include('user.includes.enquiry', ['category' => $category])

	<div class="col-md-4 col-xs-12 ff">
		<div style="background: #fff;" class="col-xs-12  f-title">
			<div class="col-md-12 "> <h4 >Related search results</h4></div>
		</div>
		@forelse (\App\Models\SubCategory::whereNotIn('scat_id',explode(',',$category))->whereIn('scat_cat_id', $mc)->limit(5)->get() as $scat)
		<a class = "related" href="{{ route('search', ['category' => $scat->scat_id]) }}">
			<div class="col-md-12 f-right ">
				<img src="{{ asset($scat->scat_img_thumb_path) }}" class="img-responsive" alt="{{ $scat->scat_title }}">
				<h4>{{ $scat->scat_title }}</h4>
			</div>
		</a>				
		@empty

		@endforelse
			{{-- <div class="col-md-12 f-right ">
				<img src="{{ asset('images/e2.jpg') }}" class="img-responsive" alt="img">
				<h4>Home Nurse</h4>
			</div>
			<div class="col-md-12 f-right ">
				<img src="{{ asset('images/e3.jpg') }}" class="img-responsive" alt="img">
				<h4>Plumber</h4>
			</div> --}}
		</div>
	</div>
</section>
@endsection