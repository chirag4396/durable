@extends('user.layouts.master')

@section('content')
<!-- about -->
<div class="section w3ls-banner-btm-main">
	{{--<div class="container">--}}
		<h2 class="heading-agileinfo sub-head">
			{{-- <p>About Us</p>
				<span>Welcome and Thank you for taking the time to check us out.</span> --}}
				<p>Welcome to Durable Facility Management</p>
				<span>We has been serving our clients for the years. After years of experiences we are continuing to improve our processes, work and throughput. We have created a good and well established company identity due to our hard work, commitment, dedicated Team work rooted on Good values. </span>
			</h2>		
			{{-- <div class="banner-btm"> --}}
			{{-- <div class="col-md-6 banner-btm-g1">
				<img src="images/s1.png" class="img-responsive" alt="" />
			</div> --}}
			{{-- <div class="col-md-12 banner-btm-g2">
				<h3 class="title-main">Welcome to Durable Facility Management </h3>

				<p>We has been serving our clients for the years. After years of experiences we are continuing to improve our processes, work and throughput. We have created a good and well established company identity due to our hard work, commitment, dedicated Team work rooted on Good values. </p>

			</div> --}}
			{{-- <div class="clearfix"></div>
		</div> --}}
	{{--</div>--}}
</div>
<!-- //about -->
<!-- offers -->
<div class="offers">
	<div class="container">

		
		<div class="offers-grids">
			<div class="col-md-12 wthree-offers-left">
				<div class="offers-left-heading">
					
				</div>
				<div class="offers-left-grids">
					<div class="offers-number">
						<p>1</p>
					</div>
					<div class="offers-text">
						<p><B>Vision</B></p> 
						<p>Services that are cheaper, safer, timeliness, standard,  more reliable and trustworthy ; services that create more job opportunities and higher incomes for workers.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="offers-left-grids offers-left-middle">
					<div class="offers-number">
						<p>2</p>
					</div>
					<div class="offers-text">
						<p><B>Mission</B></p> 
						<p>Our mission is to provide the best possible services to our customer and widen the scope of services, To become India's Largest quality service provider.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				
				<div class="offers-left-grids">
					<div class="offers-number">
						<p>3</p>
					</div>
					<div class="offers-text">
						<p><B>Purpose</B></p> 
						<p>We strive to get our clients the best quality services. We want to become a largest conglomerate platform with a variety of services as well as  increase the rate of employment is our preeminent goal. </p>
					</div>
					<div class="clearfix"> </div>
				</div>
				
				
				<div class="offers-left-grids offers-left-middle">
					<div class="offers-number">
						<p>4</p>
					</div>
					<div class="offers-text">
						<p><B>Values</B></p> 
						<p>We are grounded on the base of values and ethics.We believe that values and ethics are inevitable part of business to achieve goal and profit.We rise with our clients, Employees, registered workers and each and every stakeholder. It is our passion and social responsibility to fulfill the relationship with you. ”Your satisfaction is ours!”</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				
			</div>
			{{-- <div class="col-md-6 wthree-offers-right">

				<img src="images/s2.png" class="img-responsive" alt="" />
			</div> --}}
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!-- offers -->
@endsection