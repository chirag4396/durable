@extends('user.layouts.master')

@section('content')
<section class="fac-list">
	<div class="container fac-list1">
		<h2 class="text-center">Payment Status</h2>
		<div class="col-md-6 col-md-offset-3">
			@if ($status)
			<div class="alert">								 
				<strong>Thank you for choosing Durable Facility Management Service Pvt. Ltd.</strong>								
				<p>We recieved your payment successfully, for any query please contact us at {{ Config::get('app.mobile') }} or {{ Config::get('app.support_email') }}</p>
			</div>
			@else
			<div class="alert fail">								  
				<strong>Payment Failure!</strong> Please Try again or we'll contact to you shortly or please feel free to contact us at <b>{{ config('app.mobile') }}</b> or <b>{{ config('app.support_email') }}</b>
			</div>
			@endif
		</div>
	</div>
</section>
@endsection

@push('footer')
<script>
</script>
@endpush
