@extends('user.layouts.master')

@push('header')
@php
$cat = \App\Models\Category::find($catId);
@endphp
@endpush

@section('content')
<div class="portfolio">
	<div class="container">
		<h2 class="heading-agileinfo sub-head">
			<p>{{ $cat->cat_title }}</p>
			<span>"{{ $cat->cat_quote }}"</span>
		</h2>
		<div class="portfolio-agile">
			<div class="main">
				@if (isset($catId))
				@forelse (App\Models\SubCategory::where('scat_cat_id',$catId)->get() as $c)
				<div class="view view-seventh">
					<img src="{{ asset($c->scat_img_thumb_path) }}" alt="{{ $c->scat_title }}">
					<label for  = "cat-{{ $c->scat_id }}" onclick="$(this).find('div').toggleClass(' selected-mask');">
						<div class="mask">
							<h4>{{ 'Hire for '. $c->scat_title }}</h4>
						</div>
					</label>
					<input type="checkbox" name="cats[]" id = "cat-{{ $c->scat_id }}">
					<div>
						<h4>{{ $c->scat_title }}</h4>					
					</div>
				</div>
				@empty
				@endforelse	
				@else				
				@endif								
				<div class="clearfix"></div>
				<div class="text-center col-md-12 search-box">					
					<a href="javascript:;" id = "search" class="btn btn-search">Search</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('footer')
<script type="text/javascript">
	var searchUrl = '{{ route('search') }}';
	$('#search').on({
		'click ' : function(){
			var href = $(this).attr('href');
			if(href == "javascript:;"){
				$(this).after('<p>Please select some category first</p>');
			}
		}
	});

	$('.main').find('input').each(function(k,v){
		$(v).on({
			'change' : function(){
				var d = [];
				$('.main input[type="checkbox"]:checked').each(function(i,j){
					d.push($(j).attr('id').split('-')[1]);
				});				
				$('#search').attr('href', searchUrl+'/'+d.join(','));					
			}
		});
	});
</script>
@endpush
