<div class="w3layouts_mail_grid_right mail_grid no-padding">
	<fieldset>
		<legend>Documents Received</legend>	
		<div class="form-group col-md-3">
			<label class="control-label">Pan Card:</label>							
			<div class="text-center">
				<input type="text" class="form-control col-md-7 col-xs-12" name = "doc[pan_card_no]" autocomplete="off"  placeholder="Pan Card" id = "panCardBox" maxlength="40">
				<div class="clearfix"></div>
				<img class = "id-card" src="{{ asset('images/no-image.png') }}" id = "panCardPreview">
				<div class="clearfix"></div>
				<input type = "file" id ="panCard" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="pan_card_img">
				<label class="btn btn-success" for = "panCard">Choose Pan Card</label>
			</div>
		</div>
		<div class="form-group col-md-3">
			<label class="control-label">Aadhar Card:</label>							
			<div class="text-center">
				<input type="text" class="form-control col-md-7 col-xs-12" name = "doc[aadhar_card_no]" autocomplete="off" placeholder="Aadhar Card" id = "addCardBox" maxlength="40">
				<img class = "id-card" src="{{ asset('images/no-image.png') }}" id = "addCardPreview">
				<div class="clearfix"></div>
				<input type = "file" id ="addCard" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="add_card_img">
				<label class="btn btn-success" for = "addCard">Choose Electriciy Bill</label>
			</div>
		</div>	
		<div class="form-group col-md-3">
			<label class="control-label">Voter ID/Election Card:</label>							
			<div class="text-center">
				<input type="text" class="form-control col-md-7 col-xs-12" name = "doc[voter_no]" autocomplete="off" placeholder="Election Card" id = "voterCardBox" maxlength="40">
				<img class = "id-card" src="{{ asset('images/no-image.png') }}" id = "voterCardPreview">
				<div class="clearfix"></div>
				<input type = "file" id ="voterCard" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="voter_img">
				<label class="btn btn-success" for = "voterCard">Choose Election Card</label>
			</div>
		</div>	
		<div class="form-group col-md-3">
			<label class="control-label">Electriciy Bill:</label>							
			<div class="text-center">
				<input type="text" class="form-control col-md-7 col-xs-12" name = "doc[bill_no]" autocomplete="off" placeholder="Electriciy Bill" id = "biilBox" maxlength="40">
				<img class = "id-card" src="{{ asset('images/no-image.png') }}" id = "billPreview">
				<div class="clearfix"></div>
				<input type = "file" id ="bill" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="bill_img">
				<label class="btn btn-success" for = "bill">Choose Electriciy Bill</label>
			</div>
		</div>	
	</fieldset>
</div>	

@push('footer')
<script type="text/javascript">
	imageUpload('panCard');
	imageUpload('addCard');
	imageUpload('bill');
	imageUpload('voterCard');
</script>
@endpush