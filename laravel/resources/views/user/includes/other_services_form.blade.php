<form id = "{{ $ID }}Form">
	<input type="hidden" name="type" value="{{ $type }}">
	<div class="w3layouts_mail_grid_right mail_grid no-padding">						
		<div class="col-md-2">
			<div class="form-group">
				<label class="control-label">Employee Image:</label>
				<div class="text-center">
					<img src="{{ asset('images/blank-user.jpg') }}" width="150" height="150" id = "user-imgPreview">
					<div class="clearfix"></div>
					<input type = "file" id ="user-img" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="user_img">
					<label class="btn btn-success" for = "user-img">Choose User Image:</label>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="form-group text-center">
				<label class="control-label">Employee ID:</label>
				<label class="control-label">{{ $empId }}</label>
			</div>
		</div>
		<div class="col-md-5">
			<input type="hidden" name="emp_id" value="{{ $empId }}">
			<div>
				<label class="control-label">Worker Name:</label>
				<input type="text" class="form-control col-md-7 col-xs-12" name = "name" data-validate = "empty|alphaSpace" autocomplete="off">
				<div class="clearfix"></div>
			</div>
			<div>
				<label class="control-label">Date of Birth:</label>
				<input type="date" class="form-control col-md-7 col-xs-12" name = "dob">
				<div class="clearfix"></div>
			</div>
			<div class="mix">
				<div class="col-md-6 no-padding">
					<label class="control-label">Gender:</label>
					<div class="clearfix"></div>
					<div class="mix-inputs">							
						<input type="radio" name="gender" value = "0" checked> Male
						<input type="radio" name="gender" value = "1"> Female
					</div>						
				</div>
				<div class="col-md-6">
					<label class="control-label">Eating Food:</label>
					<div class="clearfix"></div>
					<div class="mix-inputs">
						<input type="radio" name="vegan" value = "0" checked> Yes
						<input type="radio" name="vegan" value = "1"> No
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="mix">				
				<div class="col-md-6 no-padding">
					<label class="conrol-label">Verified?:</label>
					<div class="clearfix"></div>
					<div class="mix-inputs">
						<input type="radio" name="verification" value = "1" checked> Yes
						<input type="radio" name="verification" value = "0"> No
					</div>
				</div>
				<div class="col-md-6">
					<label class="conrol-label">Marital Statu:</label>
					<div class="clearfix"></div>
					<div class="mix-inputs">
						<input type="radio" name="marital" value = "1" checked> Married
						<input type="radio" name="marital" value = "0"> Un-Married
					</div>
				</div>
				<div class="clearfix"></div>
			</div>			
			<div class="form-group">
				<label class="control-label">Category for Work:</label>

				<select class="form-control col-md-7 col-xs-12 cat" name="cat[]" multiple="multiple">
					<option value = "-1">--select--</option>
					@php
					if (!in_array($type, [1,2])){								
						$categories = App\Models\Category::whereIn('cat_id', [3,4])->get();
					}else{
						$categories = App\Models\Category::where('cat_id', $type)->get();
					}
					@endphp
					@forelse ($categories as $cat)
					<optgroup label="{{ $cat->cat_title }}"></optgroup>
					@forelse (App\Models\SubCategory::where('scat_cat_id', $cat->cat_id)->get() as $c)										
					<option value = "{{ $c->scat_id }}">{{ $c->scat_title }}</option>
					@empty										
					@endforelse
					@empty
					@endforelse									
				</select>									
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="col-md-5">			
			<div class="form-group">
				<label class="control-label">Qualification:</label>
				<select class="form-control col-md-7 col-xs-12 lang" name="qualifications[]" multiple="multiple">
					<option value = "-1">--select--</option>
					@forelse (App\Models\Qualification::get() as $q)
					<option value = "{{ $q->qua_id }}">{{ $q->qua_title }}</option>
					@empty
					@endforelse									
				</select>
				<div class="clearfix"></div>

			</div>
			<div class="form-group">
				<label class="control-label">Languages Known:</label>

				<select class="form-control col-md-7 col-xs-12 lang" name="lang[]" multiple="multiple">
					<option value = "-1">--select--</option>
					@forelse (App\Models\Language::get() as $l)
					<option value = "{{ $l->lan_id }}">{{ $l->lan_title }}</option>
					@empty
					@endforelse									
				</select>
				<div class="clearfix"></div>

			</div>
			<div class="form-group">
				<label class="control-label">Blood Group:</label>

				<select class="form-control col-md-7 col-xs-12 blood-group" name="blood_group">
					<option value = "-1">--select--</option>
					@forelse (App\Models\BloodGroup::get() as $bg)
					<option value = "{{ $bg->bg_id }}">{{ $bg->bg_title }}</option>
					@empty
					@endforelse
				</select>
				<div class="clearfix"></div>
			</div>
			<div class="form-group">
				<label class="control-label">Mother Tongue:</label>
				<select class="form-control col-md-7 col-xs-12 tongue" name="mother_tongue">
					<option value = "-1">--select--</option>
					@forelse (App\Models\Language::get() as $la)
					<option value = "{{ $la->lan_id }}">{{ $la->lan_title }}</option>
					@empty
					@endforelse
				</select>
				<div class="clearfix"></div>
			</div>		
			<div>
				<label class="control-label">Experience:</label>
				<input type="number" class="form-control col-md-7 col-xs-12" name = "maid[experience]" data-validate = "empty" autocomplete="off">
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>


	<div class="w3layouts_mail_grid_right mail_grid no-padding">
		<fieldset>
			<legend>Address:</legend>
			<div class="col-md-6">
				<div>
					<label class="control-label">Parmanent Address:</label>
					<textarea class="form-control" name="permanent_address"></textarea>
					<div class="clearfix"></div>
				</div>
				<div class="form-group">
					<label class="control-label">Parmanent Location:</label>
					<select class="form-control col-md-7 col-xs-12 p-location" name="parmanent_location">
						<option value = "-1">--select--</option>
						@forelse (App\Models\Location::get() as $l)
						<option value = "{{ $l->loc_id }}">{{ $l->loc_title }}</option>
						@empty
						@endforelse
					</select>
					<div class="clearfix"></div>
				</div>
				<div class="form-group">
					<label class="control-label">Parmanent City:</label>
					<select class="form-control col-md-7 col-xs-12 p-city" name="parmanent_city">
						<option value = "-1">--select--</option>
						@forelse (App\Models\City::get() as $c)
						<option value = "{{ $c->city_id }}">{{ $c->city_title }}</option>
						@empty
						@endforelse
					</select>
					<div class="clearfix"></div>
				</div>				
			</div>
			<div class="col-md-6">
				<div>
					<label class="control-label">Current Address:</label>
					<textarea class="form-control" name="current_address"></textarea>
					<div class="clearfix"></div>
				</div>	
				<div class="form-group">
					<label class="control-label">Current Location:</label>
					<select class="form-control col-md-7 col-xs-12 c-location" name="current_location">
						<option value = "-1">--select--</option>
						@forelse (App\Models\Location::get() as $l)
						<option value = "{{ $l->loc_id }}">{{ $l->loc_title }}</option>
						@empty
						@endforelse
					</select>
					<div class="clearfix"></div>
				</div>
				<div class="form-group">
					<label class="control-label">Current City:</label>
					<select class="form-control col-md-7 col-xs-12 c-city" name="current_city">
						<option value = "-1">--select--</option>
						@forelse (App\Models\City::get() as $c)
						<option value = "{{ $c->city_id }}">{{ $c->city_title }}</option>
						@empty
						@endforelse
					</select>
					<div class="clearfix"></div>
				</div>	
			</div>
		</fieldset>
		<div class="clearfix"></div>
	</div>

	<div class="w3layouts_mail_grid_right mail_grid no-padding">
		<fieldset>
			<legend>Contact Details</legend>			
			<div class="col-md-4">
				<label class="control-label">Mobile:</label>
				<input type="number" class="form-control col-md-7 col-xs-12" name = "mobile" data-validate = "empty|mobile" autocomplete="off" min = "7000000000" max = "9999999999">
				<div class="clearfix"></div>

			</div>
			<div class="col-md-4">
				<label class="control-label">Alternate Mobile:</label>
				<input type="number" class="form-control col-md-7 col-xs-12" name = "alt_mobile" autocomplete="off">
				<div class="clearfix"></div>

			</div>
			<div class="col-md-4">
				<label class="control-label">Email:</label>
				<input type="email" class="form-control col-md-7 col-xs-12" name = "email" data-validate = "empty|email" autocomplete="off">
				<div class="clearfix"></div>
			</div>
		</fieldset>
		<div class="clearfix"></div>
	</div>
	@include('user.includes.documents')
	<div class="clearfix"></div>
	<div class="text-center">
		<button type="submit" class="btn btn-default bton">Register</button>
	</div>
</form>