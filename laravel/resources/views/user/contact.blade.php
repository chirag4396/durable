@extends('user.layouts.master')
@push('header')
@php
$ID = 'contact';
@endphp
<script type="text/javascript">
	var ID = '{{ $ID }}';
</script>
@endpush
@section('content')
<div class="contact">
	<div class="container">
		<h2 class="heading-agileinfo">contact us<span></span></h2>
		<div class="w3layouts_mail_grids">
			<div class="col-md-4 w3layouts_mail_grid_left">
				<div class="footer-grids1f2">
					<h3>Address</h3>
					<p>Pune <br>maharashtra<br>
						Freephone: +1 234 456 7890<br>
						Telephone: +1 234 456 7890<br>
					</p>
					<p>E-mail : <a href="mailto:info@example.com">example@mail.com</a></p>
				</div>	
			</div>
			<div class="col-md-8 w3layouts_mail_grid_right">
				<form id = "{{ $ID }}Form">
					<div class="col-md-6 wthree_contact_left_grid">
						<input type="text" name="name" placeholder="Name" required data-validate = "empty|alphaSpace">
						<input type="email" name="email" placeholder="Email" required data-validate = "empty|email">
					</div>
					<div class="col-md-6 wthree_contact_left_grid">
						<input type="text" name="mobile" placeholder="Telephone" required data-validate = "empty|mobile">
						<input type="text" name="subject" placeholder="Subject" required>
					</div>
					<div class="clearfix"></div>
					<textarea name="query" placeholder="Message..." required onfocus="$(this).val('');" onblur="$(this).val('Message...');"></textarea>
					<input type="submit" value="Submit">
					<input type="reset" value="Clear">
				</form>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!-- //contact form -->
<!-- map -->	
<div class="map">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d242117.68101783327!2d73.72287860996963!3d18.524890422025432!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2bf2e67461101%3A0x828d43bf9d9ee343!2sPune%2C+Maharashtra!5e0!3m2!1sen!2sin!4v1519488764769" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<!-- //map -->	
@endsection

@push('footer')
<script type="text/javascript">
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route($ID.'.store') }}',		
		processResponse : function (data) {
			if(data.msg == "success"){
				$('#formAlert').html('We will contact you soon!!');
			}
		}
	});	
</script>
@endpush