<html>
<body>
  @if ($formError)  
  <span style="color:red">Please fill all mandatory fields.</span>
  <br/>
  <br/>
  @endif

  <form action="{{ route('pay') }}" method="post" name="Form">
    {{ csrf_field() }}
    <table>
      <tr>
        <td><b>Mandatory Parameters</b></td>
      </tr>
      <tr>
        <td>Amount: </td>
        <td><input name="amount" value="{{ $posted['amount'] or '' }}" /></td>
        <td>First Name: </td>
        <td><input name="firstname" id="firstname" value="{{ $posted['firstname'] or '' }}" /></td>
      </tr>
      <tr>
        <td>Email: </td>
        <td><input name="email" id="email" value="{{ $posted['email'] or '' }}" /></td>
        <td>Phone: </td>
        <td><input name="phone" value="{{ $posted['phone'] or '' }}" /></td>
      </tr>
      <tr>
        <td>Product Info: </td>
        <td colspan="3"><input name="productinfo" value="{{ $posted['productinfo'] or '' }}" size="64" /></td>
      </tr>
      <tr>
        <td>Success URI: </td>
        <td colspan="3"><input name="surl" value="{{ $posted['surl'] or 'http://localhost/payeasebuzz-phplib/response.php' }}" size="64" readonly /></td>
      </tr>
      <tr>
        <td>Failure URI: </td>
        <td colspan="3"><input name="furl" value="{{ $posted['furl'] or 'http://localhost/payeasebuzz-phplib/response.php' }}" size="64" readonly /></td>
      </tr>
      <tr>
        <td><b>Optional Parameters</b></td>
      </tr>
      <tr>
        <td>UDF1: </td>
        <td><input name="udf1" value="{{ $posted['udf1'] or '' }}" /></td>
        <td>UDF2: </td>
        <td><input name="udf2" value="{{ $posted['udf2'] or '' }}" /></td>
      </tr>
      <tr>
        <td>UDF3: </td>
        <td><input name="udf3" value="{{ $posted['udf3'] or '' }}" /></td>
        <td>UDF4: </td>
        <td><input name="udf4" value="{{ $posted['udf4'] or '' }}" /></td>
      </tr>
      <tr>
        <td>UDF5: </td>
        <td><input name="udf5" value="{{ $posted['udf5'] or '' }}" /></td>
      </tr>
      <tr>
        <td colspan="4"><input type="submit" value="Submit" /></td>
      </tr>
    </table>
  </form>
</body>
</html>