@extends('admin.layouts.master')
@php
$ID = 'employee';
@endphp
@push('header')
<script>
	ID = '{{ $ID }}';
</script>
<style type="text/css">
img {
	object-fit: contain;
}

</style>
@section('title')
Create {{ ucwords($ID) }}
@endsection
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endpush
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3> Register {{ ucwords($ID) }}</h3>
		</div>
		<div class="pull-right">
			<a href = "{{ route('admin.'.$ID.'.index') }}" class="btn btn-danger">Back</a>
		</div>
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">
					<form id = "{{ $ID }}Form">
						<div class="w3layouts_mail_grid_right mail_grid no-padding">						
							<div class="col-md-2">
								<div class="form-group">
									<label class="control-label">Employee Image</label>
									<div class="text-center">
										<img src="{{ asset($emp->detail ? $emp->detail->ud_img_path : '') }}" width="150" height="150" id = "user-imgPreview">
										<div class="clearfix"></div>
										<input type = "file" id ="user-img" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="user_img">
										<label class="btn btn-success" for = "user-img">Choose User Image</label>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group text-center">
									<label class="control-label">Employee ID:</label>
									<label class="control-label">{{ $emp->detail ? $emp->detail->ud_emp_id : '' }}</label>
								</div>
							</div>
							<div class="col-md-5">
								<input type="hidden" name="id" value="{{ $emp->id }}">
								<input type="hidden" name="emp_id" value="{{ $emp->detail ? $emp->detail->ud_emp_id : ''}}">
								<div>
									<label class="control-label">Full Name</label>
									<input type="text" class="form-control col-md-7 col-xs-12" name = "name" data-validate = "empty|alphaSpace" value="{{ $emp->name }}">
									<div class="clearfix"></div>
								</div>
								<div>
									<label class="control-label">Mobile</label>
									<input type="number" class="form-control col-md-7 col-xs-12" name = "mobile" data-validate = "empty|mobile" value = "{{ $emp->detail ? $emp->detail->ud_mobile : ''}}">
									<div class="clearfix"></div>

								</div>
								<div>
									<label class="control-label">Alternate Mobile</label>
									<input type="number" class="form-control col-md-7 col-xs-12" name = "alt_mobile" data-validate = "empty|mobile" value = "{{ $emp->detail ? $emp->detail->ud_alt_mobile : ''}}">
									<div class="clearfix"></div>

								</div>
								<div>
									<label class="control-label">Email</label>
									<input type="email" class="form-control col-md-7 col-xs-12" name = "email" data-validate = "empty|email" value = "{{ $emp->email }}">
									<div class="clearfix"></div>

								</div>

								<div>
									<label class="control-label">Date of Birth</label>					
									<input type="date" class="form-control col-md-7 col-xs-12" name = "dob"  value = "{{ $emp->detail ? $emp->detail->ud_dob : ''}}">
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-5 ">
								<div style="margin-bottom: 22px;">
									<label class="control-label">Gender</label>
									<div class="clearfix"></div>
									<div>										
										<input type="radio" name="gender" value = "0" {{ $emp->detail ? ($emp->detail->ud_gender ? 'checked' : '') : ''}}> Male
										<input type="radio" name="gender" value = "1" {{ $emp->detail ? ($emp->detail->ud_gender ? '' : 'checked') : '' }}> Female
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="form-group">
									<label class="control-label">Category for Work</label>
									<select class="form-control col-md-7 col-xs-12 cat" name="cat[]" multiple="multiple">
										<option value = "-1">--select--</option>
										@forelse (App\Models\Category::get() as $cat)
										<optgroup label="{{ $cat->cat_title }}"></optgroup>
										@forelse (App\Models\SubCategory::where('scat_cat_id', $cat->cat_id)->get() as $c)
										<option value = "{{ $c->scat_id }}" {{ in_array($c->scat_id, explode(',',$emp->detail->ud_category)) ? 'selected' : ''}}>{{ $c->scat_title }}</option>
										@empty										
										@endforelse
										@empty
										@endforelse									
									</select>									
									<div class="clearfix"></div>

								</div>
								<div class="form-group">
									<label class="control-label">Known Languages</label>

									<select class="form-control col-md-7 col-xs-12 lang" name="lang[]" multiple="multiple">
										<option value = "-1">--select--</option>
										@forelse (App\Models\Language::get() as $l)
										<option value = "{{ $l->lan_id }}" {{ in_array($l->lan_id, explode(',',$emp->detail->ud_language)) ? 'selected' : '' }}>{{ $l->lan_title }}</option>
										@empty
										@endforelse									
									</select>
									<div class="clearfix"></div>

								</div>
								<div class="form-group">
									<label class="control-label">Qualifications</label>

									<select class="form-control col-md-7 col-xs-12 qua" name="qualifications[]" multiple="multiple">
										<option value = "-1">--select--</option>
										@forelse (App\Models\Qualification::get() as $q)
										<option value = "{{ $q->qua_id }}" {{ in_array($q->qua_id, explode(',',$emp->detail->ud_qualification)) ? 'selected' : '' }}>{{ $q->qua_title }}</option>
										@empty
										@endforelse									
									</select>									
									<div class="clearfix"></div>

								</div>								
							</div>
						</div>
						<div class="col-md-12">
							<h3>Upload Documents</h3>
							<div class="form-group col-md-6">
								<label class="control-label">Identity Proof
								</label>							
								<div class="text-center">
									<input type="text" class="form-control col-md-7 col-xs-12" name = "identity_no" value = "{{ $emp->detail->ud_identity_no }}">
									<div class="clearfix"></div>
									<img class = "id-card" src="{{ asset($emp->detail->ud_identity_proof) }}" id = "id-cardPreview">
									<div class="clearfix"></div>
									<input type = "file" id ="id-card" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="id_card">
									<label class="btn btn-success" for = "id-card">Choose Pan Card or Aadhar Card</label>
								</div>
							</div>
							<div class="form-group col-md-6">
								<label class="control-label">Address Proof
								</label>							
								<div class="text-center">
									<input type="text" class="form-control col-md-7 col-xs-12" name = "address_no" value = "{{ $emp->detail->ud_address_no }}">
									<img class = "id-card" src="{{ asset($emp->detail->ud_address_proof) }}" id = "add-cardPreview">
									<div class="clearfix"></div>
									<input type = "file" id ="add-card" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="add_card">
									<label class="btn btn-success" for = "add-card">Choose Electriciy Bill or Election Card</label>
								</div>
							</div>	
						</div>
						<div class="clearfix"></div>
						<div class="text-center">
							<button type="submit" class="btn btn-default bton">Register</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('footer')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route('admin.'.$ID.'.index') }}',
		type:2,
		validation:false,	
		processResponse : function (data) {
			console.log(data);
		}
	});	
	function geturl(name) {
		var link = '{{ route('admin.home') }}/'+name;
		return link;
	}
	
	multiSelect('.qua', 'qualification');
	multiSelect('.lang', 'language');
	multiSelect('.cat', 'category');

	imageUpload('id-card');
	imageUpload('add-card');
	imageUpload('user-img');
</script>
@endpush