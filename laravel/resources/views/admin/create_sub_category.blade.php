@extends('admin.layouts.master')
@section('title')
Create Sub Category
@endsection

@php
	$ID = 'sub-category';
@endphp
@push('header')
<script>
	ID = '{{ $ID }}';
</script>
@endpush
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3> Create New Sub Category</h3>
		</div>
		<div class="pull-right">
			<a href = "{{ route('admin.'.$ID.'.index') }}" class="btn btn-danger">Back</a>
		</div>
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">
					<br />
					<form id = "{{ $ID }}Form" class="form-horizontal form-label-left" enctype="multipart/form-data">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Select Category
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<select class="form-control" name = "cat_id">
									<option value = "-1">Select</option>
									@foreach (App\Models\Category::get() as $cat)
									<option value = "{{ $cat->cat_id }}">{{ $cat->cat_title }}</option>
									@endforeach
								</select>								
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Sub Category Name
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="text" class="form-control col-md-7 col-xs-12" name = "title">
							</div>
						</div>						
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Sub Thumbnail
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="file" class="form-control col-md-7 col-xs-12" name = "sub_picture">
							</div>
						</div>
						<div class="ln_solid">
						</div>
						<div class="form-group text-center">							
							<button type="submit" class="btn btn-success">Add
							</button>							
						</div>					
					</form>					
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('footer')
<script>
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route('admin.'.$ID.'.store') }}',
		validation:false,	
		processResponse : function (data) {
			console.log(data);
		}
	});		
</script>
@endpush