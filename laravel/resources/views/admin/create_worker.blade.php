@extends('admin.layouts.master')
@php
$ID = 'worker';
@endphp
@push('header')
<script>
	ID = '{{ $ID }}';
</script>
<style type="text/css">
img {
	object-fit: contain;
}
</style>
@section('title')
Create {{ ucwords($ID) }}
@endsection
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endpush
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3> Register {{ ucwords($ID) }}</h3>
		</div>
		<div class="pull-right">
			<a href = "{{ route('admin.'.$ID.'.index') }}" class="btn btn-danger">Back</a>
		</div>
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">
					<form id = "{{ $ID }}Form" class="form-horizontal form-label-left" enctype="multipart/form-data">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Name
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="text" class="form-control col-md-7 col-xs-12" name = "name">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="number" class="form-control col-md-7 col-xs-12" name = "mobile">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Alternate Mobile
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="number" class="form-control col-md-7 col-xs-12" name = "alt_mobile">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Email
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="email" class="form-control col-md-7 col-xs-12" name = "email">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Date of Birth
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="date" class="form-control col-md-7 col-xs-12" name = "dob">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Gender
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="radio" name="gender" value = "0"> Male
								<input type="radio" name="gender" value = "1"> Female
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Category for Work</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<select class="form-control col-md-7 col-xs-12 cat" name="cat[]" multiple="multiple">
									<option value = "-1">--select--</option>
									@forelse (App\Models\Category::get() as $cat)
									<optgroup label="{{ $cat->cat_title }}"></optgroup>
									@forelse (App\Models\SubCategory::where('scat_cat_id', $cat->cat_id)->get() as $c)										
									<option value = "{{ $c->scat_id }}">{{ $c->scat_title }}</option>
									@empty										
									@endforelse
									@empty
									@endforelse									
								</select>									
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Known Languages</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<select class="form-control col-md-7 col-xs-12 lang" name="lang[]" multiple="multiple">
									<option value = "-1">--select--</option>
									@forelse (App\Models\Language::get() as $l)
									<option value = "{{ $l->lan_id }}">{{ $l->lan_title }}</option>
									@empty
									@endforelse									
								</select>									
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Qualifications</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<select class="form-control col-md-7 col-xs-12 qua" name="qualifications[]" multiple="multiple">
									<option value = "-1">--select--</option>
									@forelse (App\Models\Qualification::get() as $q)
									<option value = "{{ $q->qua_id }}">{{ $q->qua_title }}</option>
									@empty
									@endforelse									
								</select>									
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Identity Proof
							</label>							
							<div class="col-md-6 col-sm-6 col-xs-12 text-center">
								<input type="text" class="form-control col-md-7 col-xs-12" name = "identity_no">
								<img src="{{ asset('images/no-image.png') }}" width="300" height="150" id = "id-cardPreview">
								<div class="clearfix"></div>
								<input type = "file" id ="id-card" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="id_card">
								<label class="btn btn-success" for = "id-card">Choose Pan Card or Aadhar Card</label>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Address Proof
							</label>							
							<div class="col-md-6 col-sm-6 col-xs-12 text-center">
								<input type="text" class="form-control col-md-7 col-xs-12" name = "address_no">
								<img src="{{ asset('images/no-image.png') }}" width="300" height="150" id = "add-cardPreview">
								<div class="clearfix"></div>
								<input type = "file" id ="add-card" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="add_card">
								<label class="btn btn-success" for = "add-card">Choose Electriciy Bill or Election Card</label>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">User Image
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12 text-center">
								<img src="{{ asset('images/blank-user.jpg') }}" width="100" height="150" id = "user-imgPreview">
								<div class="clearfix"></div>
								<input type = "file" id ="user-img" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="user_img">
								<label class="btn btn-success" for = "user-img">Choose User Image</label>
							</div>
						</div>
						<div class="ln_solid">
						</div>
						<div class="form-group text-center">							
							<button type="submit" class="btn btn-success">Add
							</button>							
						</div>					
					</form>					
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('footer')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route('admin.'.$ID.'.store') }}',
		validation:false,	
		processResponse : function (data) {
			console.log(data);
		}
	});	
	function geturl(name) {
		var link = '{{ route('admin.home') }}/'+name;
		return link;
	}
	
	multiSelect('.qua', 'qualification');
	multiSelect('.lang', 'language');
	multiSelect('.cat', 'category');
		
	imageUpload('id-card');
	imageUpload('add-card');
	imageUpload('user-img');
</script>
@endpush