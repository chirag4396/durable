<?php

namespace App\Http\Controllers;

use App\Models\SubCategory;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
class SubCategoryController extends Controller
{
    use GetData;
    
    protected $path = 'images/sub-categories/';
    protected $thumbPath = 'images/sub-categories/thumbnails/';

    protected $response = ['msg' => 'error'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_sub_categories')->with(['categories' => SubCategory::orderBy('scat_title', 'asc')->get()]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_sub_category');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $regular = '';
        $thumb = '';
        $q = $this->changeKeys('scat_' , $r->all());
        if ($r->hasFile('sub_picture')) {

            list($regular, $thumb) = $this->uploadFiles($r, $q['scat_title'], 'sub_picture', [$this->path,$this->thumbPath],[], [1000,350]);
            unset($q['scat_sub_picture']);
        }
        
        $q['scat_img_path'] = $regular;
        $q['scat_img_thumb_path'] = $thumb;
        
        $cat = SubCategory::create($q);
        
        if ($cat) {
            $this->response = ['msg' => 'success', 'd' => $this->removePrefix($cat->toArray())];
        }

        return $this->response;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function show(SubCategory $subCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(SubCategory $subCategory)
    {
        return view('admin.edit_sub_category')->with(['sub'=>$subCategory]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, SubCategory $subCategory)
    {
        unset($r->_method);
        $regular = '';
        $thumb = '';

        $q = $this->changeKeys('scat_' , $r->all());
        if ($r->hasFile('sub_picture')) {
            $this->removeFile($subCategory->scat_img_path);            
            list($regular, $thumb) = $this->uploadFiles($r, $q['scat_title'], 'sub_picture',  [$this->path,$this->thumbPath],[], [1000,350]);
            unset($q['scat_sub_picture']);
        }
        
        $q['scat_img_path'] = $regular;
        $q['scat_img_thumb_path'] = $thumb;

        $w = $subCategory->update($q);
        if ($w) {
            
            $this->response['msg'] = 'successU';
        }
        return $this->response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategory $subCategory)
    {
        // return $submit;
        $this->removeFile($subCategory->scat_img_path);
        $subCategory->delete();

        return redirect()->back();
    }
}
