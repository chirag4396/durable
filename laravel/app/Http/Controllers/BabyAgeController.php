<?php

namespace App\Http\Controllers;

use App\Models\BabyAge;
use Illuminate\Http\Request;

class BabyAgeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BabyAge  $babyAge
     * @return \Illuminate\Http\Response
     */
    public function show(BabyAge $babyAge)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BabyAge  $babyAge
     * @return \Illuminate\Http\Response
     */
    public function edit(BabyAge $babyAge)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BabyAge  $babyAge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BabyAge $babyAge)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BabyAge  $babyAge
     * @return \Illuminate\Http\Response
     */
    public function destroy(BabyAge $babyAge)
    {
        //
    }
}
