<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\UserDetail;
use App\Models\Document;
use App\Models\MaidDetail;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class UserDetailController extends Controller
{
    use GetData;

    protected $response = ['msg' => 'error'];
    
    protected $ud_id;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_worker');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function images(Request $r)
    {        
        // return count($r->user_img);
        // foreach ($r->user_img as $k => $value) {         
        list($identity) = $this->uploadFiles($r, 'svm'.rand(9999,2), 'user_img', ['images/others/'], [], 1600);
        // }
    }
    public function store(Request $r)
    {
        // return $r->all();
        $pan_card = 'images/no-image.png';
        $aadhar_card = 'images/no-image.png';
        $voter_id = 'images/no-image.png';
        $bill = 'images/no-image.png';        
        $user_img = 'images/blank-user.jpg';
        if (UserDetail::where('ud_email', $r->email)->first()) {
            $this->response['msg'] = 'exist';            
            return $this->response;
        }
        
        if ($r->hasFile('pan_card_img')) {
            list($pan_card) = $this->uploadFiles($r, $r->name, 'pan_card_img', ['images/pan-cards/']);
            unset($r->pan_card_img);
        }
        if ($r->hasFile('add_card_img')) {
            list($aadhar_card) = $this->uploadFiles($r, $r->name, 'add_card_img', ['images/aadhar-cards/']);
            unset($r->add_card_img);
        }
        if ($r->hasFile('voter_img')) {
            list($voter_id) = $this->uploadFiles($r, $r->name, 'voter_img', ['images/voter-ids/']);
            unset($r->voter_img);
        }
        if ($r->hasFile('bill_img')) {
            list($bill) = $this->uploadFiles($r, $r->name, 'bill_img', ['images/bills/']);
            unset($r->bill_img);
        }
        if ($r->hasFile('user_img')) {            
            list($user_img) = $this->uploadFiles($r, $r->name, 'user_img', ['images/users/']);
            unset($r->user_img);
        }

        $ud = $this->changeKeys('ud_',$r->all());        

        $ud['ud_emp_id'] = $ud['ud_emp_id'];        
        $ud['ud_img_path'] = $user_img;
        $ud['ud_category'] = implode(',', $ud['ud_cat']);
        $ud['ud_language'] = isset($ud['ud_lang']) ? implode(',', $ud['ud_lang']) : '';
        $ud['ud_qualification'] = isset($ud['ud_qualifications']) ? implode(',', $ud['ud_qualifications']) : '';

        unset($ud['ud_lang']);
        unset($ud['ud_qualifications']);
        unset($ud['ud_cat']);        
        $doc = $this->changeKeys('doc_',$r->all()['doc']);
        unset($ud['ud_doc']);
        try {
            $udetail = UserDetail::create($ud);            
            $this->ud_id = $udetail->ud_id;
            $doc['doc_pan_card_proof'] = $pan_card;
            $doc['doc_aadhar_card_proof'] = $aadhar_card;
            $doc['doc_voter_id_proof'] = $voter_id;
            $doc['doc_bill_proof'] = $bill;
            $doc['doc_user'] = $udetail->ud_id;
            Document::create($doc);
            
            switch ($r->type) {
                case 1:
                $maid = $this->changeKeys('maid_',$r->all()['maid']);
                
                $maid['maid_cleaning'] = $r->cleaning ? implode(',',$r->cleaning) : '';
                // $maid['maid_baby_ages'] = implode(',',$r->ages);
                $maid['maid_baby_sitting'] = $r->baby ? implode(',',$r->baby) : '';
                $maid['maid_elder_care'] = $r->elderly ? implode(',',$r->elderly) : '';
                $maid['maid_patient_care_service'] = $r->patient_care ? implode(',',$r->patient_care) : '';
                $maid['maid_work_hours'] = $r->working ? implode(',',$r->working) : '';
                $maid['maid_dishes'] = $r->dishes ? implode(',',$r->dishes) : '';
                // $maid['maid_cooking_type'] = implode(',',$r->food_type);
                $maid['maid_cooking_region'] = $r->food ? implode(',',$r->food) : '';
                // $maid['maid_preferred_location'] = implode(',',$r->location);
                $maid['maid_user'] = $udetail->ud_id;
                MaidDetail::create($maid);        
                break;
            }
            
            if($udetail){
                $this->response['msg'] = 'success';
            }
        } catch (QueryException $e) {
            UserDetail::find($this->ud_id)->delete();
            $this->response['error'] = $e->getMessage();
        }            

        return $this->response;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserDetail  $userDetail
     * @return \Illuminate\Http\Response
     */
    public function show(UserDetail $userDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserDetail  $userDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(UserDetail $userDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserDetail  $userDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserDetail $userDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserDetail  $userDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserDetail $userDetail)
    {
        //
    }
}
