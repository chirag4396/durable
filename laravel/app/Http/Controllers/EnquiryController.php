<?php

namespace App\Http\Controllers;

use App\Models\Enquiry;
use App\Models\Company;
use App\Models\SubCategory;
use App\Models\UserDetail;
use App\Models\Otp;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;

class EnquiryController extends Controller
{
    use GetData;

    protected $response = ['msg' => 'error'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_enquiries')->with(['enquiries' => Enquiry::get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        // return $r->all();
        $c = 0;
        $old = Company::where('cmp_email', $r->email)->first();
        if(!$old){
            $cmp = $this->changeKeys('cmp_', $r->all());
            unset($cmp['cmp_user']);
            unset($cmp['cmp_from']);
            unset($cmp['cmp_to']);
            unset($cmp['cmp_query']);

            $c = Company::create($cmp);
        }else{
            $c = $old;
        }
        if ($c) {        
            $enq = $this->changeKeys('enq_', $r->all());
            $enq['enq_company'] = $c->cmp_id;
            $e = Enquiry::create($enq);

            if($e){
                $this->response = ['msg' => 'successEnquiry'];
            }
        }

        return $this->response;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */
    public function show(Enquiry $enquiry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */

    public function edit(Enquiry $enquiry)
    {
        $en = $enquiry->join('companies', 'companies.cmp_id', '=', 'enquiries.enq_company')->first()->toArray();
        $e = $this->removePrefix($en);
        $s = SubCategory::whereIn('scat_id', explode(',', $enquiry->enq_category))->get();
        $d = [];
        foreach ($s as $k => $v) {
            $d[] = $v->scat_title;
        }
        $e['scat_title'] = implode(', ',$d);
        return $e;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Enquiry $enquiry)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Enquiry  $enquiry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enquiry $enquiry)
    {
        //
    }

    public function otp(Request $r)
    {        
        $mobile = $r->mobile;
        $digit = rand(999999,6);
        $otp = Otp::where('otp_mobile', $mobile)->first();
        
        if ($otp) {
            $otp->delete();
        }

        $new = Otp::create([
            'otp_digit' => $digit,
            'otp_mobile' => $mobile,
        ]);
        
        if ($new) {                
            $this->sendSMS($r->mobile,$digit);
            $this->response['msg'] = 'success';
        }
        
        return $this->response;
    }

    public function checkOtp(Request $r)
    {
        // return $r->all();
        $mobile = $r->mobile;
        $digit = $r->otp;
        $otp = Otp::where(['otp_mobile' => $mobile, 'otp_digit' => $digit])->first();
        if ($otp) {
            $otp->delete();
            $u = Company::where('cmp_mobile', $mobile)->first();
            if ($u) {            
                $user = $this->removePrefix($u->toArray());
                $this->response['user'] = $user;
            }else{
                $this->response['user'] = 'new';
            }
            $this->response['msg'] = 'success';
        }

        return $this->response;
        
    }
}
