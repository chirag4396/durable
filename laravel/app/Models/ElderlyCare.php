<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ElderlyCare extends Model
{
    protected $primaryKey = 'ec_id';

    protected $fillable = ['ec_title'];

    public $timestamps = false;
}
