<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BabySittingType extends Model
{
    protected $primaryKey = 'bst_id';

    protected $fillable = ['bst_title'];

    public $timestamps = false;
}
