<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
	protected $primaryKey = 'd_id';

	protected $fillable = ['d_title'];

	public $timestamps = false;
}
