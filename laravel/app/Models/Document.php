<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
	protected $primaryKey = 'doc_id';

	protected $fillable = [
		'doc_pan_card_no', 'doc_aadhar_card_no', 'doc_voter_no', 'doc_bill_no', 'doc_pan_card_proof', 'doc_aadhar_card_proof', 'doc_voter_id_proof', 'doc_bill_proof', 'doc_user'
	];

	public $timestamps = false;

	public function userDetail(){
		return $this->belongsTo(\App\Models\UserDetail::class, 'doc_user');
	}
}
