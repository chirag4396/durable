<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaidDetail extends Model
{
    public $primaryKey = 'maid_id';

    protected $fillable = ['maid_user', 'maid_cleaning', 'maid_baby_sitting', 'maid_baby_ages', 'maid_patient_care_service', 'maid_elder_care', 'maid_cooking_region', 'maid_preferred_location', 'maid_experience', 'maid_work_hours', 'maid_dishes'];
// 'maid_cooking_type'
    public $timestamps = false;

    public function userDetail(){
    	return $this->belongsTo(\App\Models\UserDetail::class, 'maid_user');
    }
}
