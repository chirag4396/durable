<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientCareService extends Model
{
    protected $primaryKey = 'pcs_id';

    protected $fillable = ['pcs_title'];

    public $timestamps = false;
}
