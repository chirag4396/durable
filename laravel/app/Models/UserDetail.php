<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{

	protected $primaryKey = 'ud_id';

	protected $fillable = [
		// 'ud_user',
		'ud_emp_id',
		'ud_name',
		'ud_age',
		'ud_email',
		'ud_gender',
		'ud_category',
		'ud_qualification',
		'ud_language',
		'ud_img_path',
		'ud_dob',
		'ud_mobile',
		'ud_verification',
		'ud_alt_mobile',
		'ud_pan_card_no',
		'ud_pan_card_proof',
		'ud_aadhar_card_no',
		'ud_aadhar_card_proof',
		'ud_voter_no',
		'ud_voter_id_proof',
		'ud_bill_no',
		'ud_bill_proof',
		'ud_permanent_address',
		'ud_permanent_city',
		'ud_permanent_location',
		'ud_current_address',
		'ud_current_city',
		'ud_current_location',
		'ud_mother_tongue',
		'ud_blood_group',
		'ud_height_feet',
		'ud_height_inches',
		'ud_chest_size',
		'ud_weight',
		'ud_marital',
		'ud_joinig_date'
	];

	CONST CREATED_AT = 'ud_created_at';

	CONST UPDATED_AT = 'ud_updated_at';

	// public function user(){
	// 	return $this->belongsTo(\App\User::class, 'ud_user');
	// }
	public function document(){
		return $this->hasOne(\App\Models\Document::class, 'doc_user');
	}

	public function maids(){
		return $this->hasMany(\App\Models\MaidDetail::class, 'maid_user');
	}
}
