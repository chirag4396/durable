<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BabyAge extends Model
{
    protected $primaryKey = 'ba_id';

    protected $fillable = ['ba_title'];

    public $timestamps = false;
}
