<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $primaryKey = 'cat_id';

    protected $fillable = ['cat_title', 'cat_img_path', 'cat_type', 'cat_quote'];

    public $timestamps = false;

    public function subCategory()
    {
    	return $this->hasMany(\App\Models\SubCategory::class, 'scat_cat_id');
    }
}
