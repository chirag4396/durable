<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
	protected $primaryKey = 'otp_id';

	protected $fillable = ['otp_digit', 'otp_mobile'];

	public $timestamps = false;
}
