<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FoodRegion extends Model
{
    protected $primaryKey = 'fr_id';

    protected $fillable = ['fr_title'];

    public $timestamps = false;
}
