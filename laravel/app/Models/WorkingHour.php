<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkingHour extends Model
{
        protected $primaryKey = 'wh_id';

        protected $fillable = ['wh_title'];

    	public $timestamps = false;
}
