<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $primaryKey = 'loc_id';

    protected $fillable = ['loc_title'];

    public $timestamps = false;
}
