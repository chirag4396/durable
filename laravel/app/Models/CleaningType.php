<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CleaningType extends Model
{
    protected $primaryKey = 'ct_id';

    protected $fillable = ['ct_title'];

    public $timestamps = false;
}
