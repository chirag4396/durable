<?php

// Route::get('/', )->name('home');

Auth::routes();
// Route::get('/pay', 'PaymentController@pay');
// Route::get('/pay', 'EaseBuzzController@epay');
// Route::post('/pay', 'EaseBuzzController@easePay')->name('pay');

Route::get('/', 'HomeController@index')->name('home');
Route::get('contact/', 'HomeController@contact')->name('contact');
Route::get('about/', 'HomeController@about')->name('about');
Route::get('select-register', 'HomeController@select_register')->name('select-register');
Route::get('job-register/{type?}/{name?}', 'HomeController@job_register')->name('job-register');
// Route::get('job-register/{}', 'HomeController@job_register')->name('job-register');
Route::get('/search-for/{id}/{name?}', 'HomeController@search')->name('home-search');
Route::post('send-otp/', 'EnquiryController@otp')->name('otp');
Route::post('check-otp/', 'EnquiryController@checkOtp')->name('check-otp');
Route::post('check-otp/', 'EnquiryController@checkOtp')->name('check-otp');

Route::get('search/{category?}/{from?}/{to?}', 'WorkerController@search')->name('search');
Route::resource('enquiry', 'EnquiryController');
Route::resource('contact', 'ContactController');

Route::get('/pay','PaymentController@getPay')->name('get-pay');

Route::post('/paydemo','PaymentController@getPay')->name('pay');

Route::post('/placeorder','PaymentController@getPay')->name('placeorder');

Route::post('/paystatus','PaymentController@orderStatus')->name('orderStatus');

Route::resource('payment', 'PaymentController');
Route::post('/placeorder','PaymentController@getPay')->name('placeorder');	
Route::post('/paystatus','PaymentController@orderStatus')->name('orderStatus');
Route::get('/booked/order/{id}/{status?}', 'PaymentController@previous')->name('previous');

Route::resource('qualification', 'QualificationController');
Route::resource('blood-group', 'BloodGroupController');
Route::resource('language', 'LanguageController');
Route::resource('category', 'CategoryController');
Route::resource('cleaning', 'CleaningTypeController');
Route::resource('baby-sitting', 'BabySittingTypeController');
Route::resource('location', 'LocationController');
Route::resource('city', 'CityController');
Route::resource('food-region', 'FoodRegionController');
Route::resource('elderly-care', 'ElderlyCareController');
Route::resource('dish', 'DishController');

Route::get('/admin/login', function() {
	return view('admin.login');
});

Route::resource('worker', 'UserDetailController');

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function() {
	Route::get('/', function(){
		return view('admin.dashboard');
	})->name('home');

	Route::get('images', function (){
		
		return view('admin.upload');
	})->name('g-images');
	Route::resource('qualification', 'QualificationController');
	Route::resource('language', 'LanguageController');
	Route::resource('blood-group', 'BloodGroupController');
	Route::resource('category', 'CategoryController');
	Route::post('images', 'UserDetailController@images')->name('images');
	Route::resource('sub-category', 'SubCategoryController');
	Route::resource('enquiry', 'EnquiryController');	
	Route::resource('contact', 'ContactController');
	Route::resource('payment', 'PaymentController');
	Route::resource('employee', 'WorkerController');

});